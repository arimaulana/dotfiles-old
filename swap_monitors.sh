#!/bin/bash


## script to toggle between monitor modes

currentmonitor=$(xrandr | awk '/\ connected/ && /[[:digit:]]x[[:digit:]].*+/{print $1}')
double=$'DisplayPort-0\nHDMI-A-0'
right=$'DisplayPort-0'
left=$'HDMI-A-0'

if [ "$currentmonitor" = "$double" ]; then
    xrandr --output $left --auto --primary --output $right --off
elif [ "$currentmonitor" = "$right" ]; then
    xrandr --output $left --auto --primary --output $right --auto --right-of $left
else
    xrandr --output $left --auto --primary --output $right --auto --right-of $left
fi

exit 0
