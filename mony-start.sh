#!/bin/bash

# start pm2
pm2 resurrect

# start smartgit
/usr/share/smartgit/bin/smartgit.sh &

# start telegram
~/Downloads/Telegram/Telegram &

# stop some pm2 service
pm2 stop 2 3 10 12 13 15
